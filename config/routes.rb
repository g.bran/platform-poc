Rails.application.routes.draw do
  root to: 'home#index'
  namespace :api do
    namespace :v1 do
      resources :fundraise_offers
    end
  end
  match '*path', to: 'home#index', via: :get
end
